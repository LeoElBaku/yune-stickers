<img width="200" src="https://gitlab.com/LeoElBaku/yune-stickers/-/raw/master/Icons/icon-A.png" />
<img width="200" src="https://gitlab.com/LeoElBaku/yune-stickers/-/raw/master/Icons/icon-B.png" />
<img width="200" src="https://gitlab.com/LeoElBaku/yune-stickers/-/raw/master/Icons/icon-C.png" />

# Yunne Stickers 

A collection of stickers for Social Apps based on the Japanese Idol Yune Sakurai 

## Who is Yune Sakurai (櫻井 佑音)?

Yune Sakurai is an upcoming young idol from Japan. At 16 years old, she’s already a talented solo signer and prize-winning child fashion model. She has frequent live performances, broadcasts a regular live stream and models for photo-shoots and fashion labels. Yune has released two CD singles and appeared in large events overseas. She also appears in film and dance videos. Check more about her below:

[sakuraiyune.com](https://www.sakuraiyune.com)  **|** [@yune_yune_go_go](https://twitter.com/yune_yune_go_go)

## In this repository
Below you will find a table of an suggested emoji and its corresponding sticker available in this repository

| Emoji  | Sticker Number | Description  |
| ------ | -------------- | ------------ |
| 🙄 | 01.png | Face With Rolling Eyes |
| 🙏🏻 | 02.png | Folded Hands |
| 👶🏻 | 03.png | Baby |
| 👧🏻 | 04.png | Girl: Light Skin Tone |
|  | 05.png | |
|  | 06.png | |
|  | 07.png | |
|  | 08.png | |
|  | 09.png | |
|  | 10.png | |
|  | 11.png | |
|  | 12.png | |
|  | 13.png | |
|  | 14.png | |
|  | 15.png | |
|  | 16.png | |
|  | 17.png | |
|  | 18.png | |
|  | 19.png | |
|  | 20.png | |
|  | 21.png | |
|  | 22.png | |
|  | 23.png | |
|  | 24.png | |
|  | 25.png | |
| 😐 | 26.png | Neutral Face |
|  | 27.png | |
|  | 28.png | |
|  | 29.png | |
|  | 30.png | |
|  | 31.png | |
| 🤥 | 32.png | Lying Face |
|  | 33.png | |
| 🤘🏻 | 34.png | Sign Of The Horns |
|  | 35.png | |
|  | 36.png | |
|  | 37.png | |
|  | 38.png | |
| 😒 | 39.png | Unamused Face |
|  | 40.png | |
| 👓 | 41.png | Glasses |
|  | 42.png | |
|  | 43.png | |
|  | 44.png | |
|  | 45.png | |
|  | 46.png | |
|  | 47.png | |
|  | 48.png | |
|  | 49.png | |
|  | 50.png | |

## Disclaimer

The multimedia content published in this repository was created exclusively for recreational or communicative purposes, `any digital or physical reproduction of it for economic purposes is explicitly prohibited as well as its distribution for purposes other than recreational or recreational communication`, or any interaction that pretend to attempt against the honor and good name of Yune in any way, be it obscene or vulgar.


